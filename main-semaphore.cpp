
#include <iostream>
#include <string>
#include <chrono>
#include <algorithm>
#include <boost/simulation.hpp>
#include "atomic-semaphore.hpp" // Direccion relativa desde la carpeta de este archivo
// ../ vuelve una carpeta anterior a la hora de poner relaciones relaticas y mezclando ../ con los nombres de los directorios llegas al archivo.

using namespace std;
using namespace boost::simulation;
using namespace boost::simulation::pdevs;
using namespace boost::simulation::pdevs::basic_models;


using hclock=chrono::high_resolution_clock;
using Time = int;
using Message = string;

int main(){
    cout << "Creating the semaphore atomic model" << endl;

    auto traffic_ligth = make_atomic_ptr<semaphore<Time, Message>, string, Time, Time, Time>(string("red"), Time(10), Time(20), Time(30)); // instancio mis atomics

    // Creacion del coupled model
    shared_ptr<flattened_coupled<Time, Message>> traffic_ligth_coupled(new flattened_coupled<Time, Message>(
    {traffic_ligth}, // lista de los modelos en el coupled
    {traffic_ligth}, // lista de EIC
    {}, // lista de IC
    {traffic_ligth}// Lista de EOC
  ));
    
    
    


    cout << "Preparing runner" << endl;
    Time initial_time{0}; // este es la variable en la que defino el momento en el tiempo en el que mi modelo inicia.
    runner<Time, Message> r(traffic_ligth_coupled, initial_time, cout, [](ostream& os, Message m){ os << m;});
    // parametro 1 es la variable donde guarde el modelo general.
    // parametro 2 es el momento del tiempo inicial en que el modelo empieza
    // parametro 3 es la variable con de tipo de datos que yo elija, que se va a encargar de guardar/procesar los mensajes de salida del modelo general.
    // parametro 4 es la funcion que toma una variable del tipo del parametro 3 y una variable del tipo de mensajes de mi modelo general, y define como el tipo del parametro 3 se encarga de guardar/procesar/mostrar... la variable mensaje que es el output del modelo general.
    Time end_time{500};

    cout << "Starting simulation until time: " << end_time << "seconds" << endl;

    auto start = hclock::now(); //to measure simulation execution time

    end_time = r.runUntil(end_time); // esto le indica al simulador que quiero que corra la simulacion entre los tiempos de inicio y el tiempo especificado en la variable end_time que tiene que ser mayor o igual a la de inicio.
    // r.runUntiPasivate();  --> corre hasta que el advance time de mi modelo (osea, el advance time de todos los submodelos de mi modelo, es infinito, cuando eso pasa, es obvio que nunca mas nadie va a hacer nada, ni enviar mensaje ergo, todo termino) sea infinito
    auto elapsed = chrono::duration_cast<std::chrono::duration<double, std::ratio<1>>> (hclock::now() - start).count(); // no hay que modificarlo, es solo para saber la duracion en tiempo real que tarda toda la simulacion en correr.
    
    cout << "Finished simulation with time: " << end_time << "sec" << endl;
    cout << "Simulation took: " << elapsed << "sec" << endl;
    return 0;
}

