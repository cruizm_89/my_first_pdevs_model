#ifndef BOOST_SIMULATION_PDEVS_BM_SEMAPHORE_H // Para cada archivo un nombre diferente y dentro de cada archivo siempre el mismo
#define BOOST_SIMULATION_PDEVS_BM_SEMAPHORE_H

#include <string>
#include <assert.h>
#include <boost/simulation/pdevs/atomic.hpp>

using namespace boost::simulation::pdevs;
using namespace boost::simulation;
using namespace std;
// usar los namespace es para no incluir los dos puntitos cada vez que tengo que usar algo que pertenece a ese namespace, cuidado con las ambiguedades

// el template me dice que a la hora de definir la clase, el tipo de datos que recibo lo define el usuario al instanciar.
// MSG tiene que ser el mismo tipo para todo mis modelos.
template<class TIME, class MSG> 
class semaphore : public pdevs::atomic<TIME, MSG> {
// En general public pdevs::atomic<TIME, MSG> no tengo que cambiarlo
private:

    TIME    _period;
    TIME    _in_yellow;
    TIME    _in_red;
    TIME    _in_green;
    string  _state;
    bool    _showing;
    //puedo definir muchas variables, incluyendo TIME y MSG
    // las variables de barra baja son para indicar que es una variable de mi clase y no de funcion o global o otro tipo de variable

public:
   
    explicit semaphore (string s, TIME in_yellow, TIME in_red, TIME in_green) noexcept {

        _state      = s;
        _in_yellow  = in_yellow;
        _in_red     = in_red;
        _in_green   = in_green;
        _showing    = true;

        if (_state == string("green")) {

            _period = _in_green;
        } else if (_state == string("yellow")) {
            
            _period = _in_yellow;
        } else if (_state == string("red")) {
            
            _period = _in_red;
        }
    }
    // la vida de los parametros y variables definidas en la funcion mueren al terminar la funcion
   

    void internal() noexcept {

        if (!_showing) {

            if (_state == string("green")) {
                
                _state  = "yellow";
                _period = _in_yellow;
            } else if (_state == string("yellow")) {
                
                _state   = "red";
                _period = _in_red;
            } else if (_state == string("red")) {
                
                _state   = "green";
                _period = _in_green;
            }
        }

        _showing = !_showing;
    }
   
    TIME advance() const noexcept {
        TIME result;

        if (_showing)   result = TIME(0);
        else            result = _period;

        return result; 
    }
   
    vector<MSG> out() const noexcept { 
        //En DEVS la funcion de salida no me .modifica nada, solo envia mensaje, por tanto al programar tiene que ser const
        // la funcion out es const por que no tengo que modificar ninguna variable de mi clase, eso no quiere decir, que no pueda modificar cualquier otro tipo de variable como result que es local de esta funcion y no de la clase.
        vector<MSG> result; // Como lo creo aca, es una variable local de la funcion, ergo su vida muere al finalizar, y solo va a tener el UNICO push back mustrado dentro del if, solo si esty en _showing. Si no no entra al if no hace el push, sigue vacio, envio un vector vacio.
        // Si no quiero enviar nada, envio un vector vacio.
        if (_showing) {
            result.push_back(_state);
        }

        return result;
    }
        
    void external(const vector<MSG>& mb, const TIME& t) noexcept { assert(false && "No external input is expected by this model"); }

  
    void confluence(const vector<MSG>& mb, const TIME& t)  noexcept  { assert(false && "No external input is expected by this model"); }

};

#endif // BOOST_SIMULATION_PDEVS_BM_SEMAPHORE_H
